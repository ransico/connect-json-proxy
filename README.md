Intro
-----

This is a simple package that wraps the _json-proxy_ package, providing support for multiple endpoints based on an HTTP Header or URL parameter.

It fulfils a specific need for internal applications for us, so is NOT designed to be super high performance or on production sites. (for example, it re-initializes the proxy for EVERY request).

Usage
-----

Assuming that grunt-connect us running on port 9000, and you can call the following URL to get the connect server to redirect the request to somewhere else.

    http://localhost:9000/PortalApp/api/v1/some/service/call?apiProxy=LocalhostNode

_redirects to the endpoint defined by LocalhostNode (ie, port 8080)..._

    http://localhost:8080/PortalApp/api/v1/some/service/call?apiProxy=LocalhostNode

Setup
-----

In your Gruntfile.js

    var apiProxy = require('grunt-json-proxy').initialize({
        // A 'node' is referenced by name in the 'proxy.forward' section below, instead of a URL.
        nodes: {
            ihportal: {
                // Each endpoint is listed here as in key: endpoint format.
                'Localhost': 'http://localhost:8080/'
            }
        },

        // If no endpoint name is passed in a http header or url parameter, use this one.
        defaultEndpointName: 'Localhost',

        // Proxy's, as defined by json-proxy, however the value of each 'forward' entry is a 'node' (as defined above) instead of a url.
        proxy: {
            forward: {
                '/PortalApp/': 'ihportal'
            }
        }
    });


    // Define the configuration for all the tasks
    grunt.initConfig({

        // The actual grunt server settings
        connect: {
            options: {
                port: 9000,
                // Change this to '0.0.0.0' to access the server from outside.
                hostname: 'localhost',
                livereload: 35729
            },
            livereload: {
                options: {
                    middleware: function (connect) {
                        return [
                            connect.static(appConfig.app),


                            // The apiProxy middleware is place HERE
                            apiProxy


                            ]
                        }
                    }
                }
            }
        });