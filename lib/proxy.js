'use strict';

var proxy = require('json-proxy'),
    _ = require('lodash');

// Add the .status() and .json() function (from Express) that the proxy middleware assumes is there.
var extendRes = function(res) {
    res.status = function(statusCode) {
        this.statusCode = statusCode;
        return this;
    };
    res.json = function(obj){
        // allow status / body
        if (2 === arguments.length) {
            // res.json(body, status) backwards compat
            if ('number' === typeof arguments[1]) {
                this.statusCode = arguments[1];
            } else {
                this.statusCode = obj;
                obj = arguments[1];
            }
        }

        // settings
        var body = JSON.stringify(obj);

        // content-type
        this.charset = this.charset || 'utf-8';
        this.setHeader('Content-Type', this.getHeader('Content-Type') || 'application/json');
        return this.end(body);
    };

    // This little hack prevents the browser from showing up the basic auth dialog on a 401 :)
    var oldwriteHead = res.writeHead;
    res.writeHead = function(statusCode) {
        if (this._headers['www-authenticate']) {
            this.setHeader('www-authenticate', null);
        }
        oldwriteHead.call(this, statusCode);
    };
    return res;
};

exports.initialize = function(options) {

    var nodes = options.nodes || {},
        defaultEndpointName = options.defaultEndpointName || 'localhost';

    // Re-jig the proxy forward options
    if (!options.proxy || !options.proxy.forward) {
        console.log('grunt-json-proxy: Did you forget to define proxy.forward configuration?');
        options.proxy = {
            forward: {}
        };
    }

    return function(req, res, next) {
        res = extendRes(res);

        try {
            var apiProxyParamMatch = req.url.match(/apiProxy=([^&]*)/);
            var proxyDest = defaultEndpointName,
                targetNodeParam = (apiProxyParamMatch ? apiProxyParamMatch[1] : ''),
                targetNodeHdr = req.headers['target-node'] || targetNodeParam;

            if (targetNodeHdr) {
                proxyDest = targetNodeHdr;
                for (var nodeKey in nodes) {
                    if (!nodes[nodeKey][proxyDest]) {
                        return res.json(500, { error: 500, message: 'Unknown node name: ' + req.headers['target-node'] });
                    }
                }
            }

            // Make a copy of the object
            var instanceOptions = {
                proxy: _.assign({}, options.proxy)
            };
            for (var nodeKey in options.proxy.forward) {
                if (!nodes[options.proxy.forward[nodeKey]]) {
                    console.error('Node key ' + nodeKey + ' does not exist in options.proxy.forward');
                }
                instanceOptions.proxy.forward = {};
                instanceOptions.proxy.forward[nodeKey] = nodes[options.proxy.forward[nodeKey]][proxyDest];
            }

            // Not very efficient, but proxy lib doesn't support multiple instances. Have to call initialize each time that proxyDest changes
            proxy.initialize(instanceOptions)(req, res, next);

        } catch(ex) {
            var exMsg = ex.message || ex;
            console.error(exMsg);
            res.json(500, { error: 500, message: 'Unexpected Exception!', exception: exMsg });
        }
    }


};